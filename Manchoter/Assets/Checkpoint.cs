﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private GameObject playerSpawn;

    private void Awake()
    {
        playerSpawn = GameObject.FindGameObjectWithTag("Respawn");


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerSpawn.transform.position = transform.position;
            Destroy(gameObject);
        }
    }
}
