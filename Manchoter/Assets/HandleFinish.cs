﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleFinish : MonoBehaviour
{

    public GameObject UI;
    public static HandleFinish instance;

    public void Awake()
    {
        if (instance != null)
        {
            print("There is more than one instance of HandleFinish");
            return;
        }
        instance = this;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            UI.SetActive(true);
            CharacBehavior charachBehavior = collision.GetComponent<CharacBehavior>();
            charachBehavior.StopPlayer();
            charachBehavior.enabled = false;
        }
    }
}
