﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public GameObject UI;
    public static GameOverManager instance;

    public void Awake()
    {
        if (instance != null)
        {
            print("There is more than one instance of GameOverManager");
            return;
        }
        instance = this;
    }

    public void OnPlayerDeath()
    {
        UI.SetActive(true);
    }

    public void RetryButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        UI.SetActive(false);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
