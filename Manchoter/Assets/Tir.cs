﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tir : MonoBehaviour
{
    public GameObject projectile;
    public int force;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity);
            bullet.GetComponent<Rigidbody2D>().velocity = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacBehavior>().TirDirection * force * Time.deltaTime;
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<CharacBehavior>().TirDirection.Equals(Vector2.left))
            {
                bullet.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                bullet.GetComponent<SpriteRenderer>().flipX = true;
            }
            Destroy(bullet, 1f);
        }
    }
}
