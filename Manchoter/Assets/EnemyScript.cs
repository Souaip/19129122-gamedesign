﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{

    public float speed;
    public Transform[] waypoints;
    // Start is called before the first frame update
    private Transform target;
    private int destPoint = 0;
    public SpriteRenderer spriteRenderer;
    void Start()
    {
        target = waypoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) < 0.3f)
        {
            destPoint = (destPoint + 1) % waypoints.Length;
            target = waypoints[destPoint];
        }

        Flip(destPoint);
    }

    void Flip(int _destpoint)
    {
        if (_destpoint > 0)
        {
            spriteRenderer.flipX = true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            PlayerHealth player = collision.transform.GetComponent<PlayerHealth>();

            player.TakeDamage(1);
        }

        if (collision.transform.CompareTag("Bullet"))
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
