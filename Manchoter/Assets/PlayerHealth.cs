﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    public int maxHealth = 3;
    public int currentHealth;
    public HealthBar healthBar;
    public bool isInvicible = false;
    public SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            TakeDamage(1);
        }
    }

    public void TakeDamage(int damage)
    {
        if (!isInvicible)
        {
            currentHealth -= damage;
            healthBar.setHealth(currentHealth);
            if (currentHealth <= 0)
            {
                Die();
                return;
            }
            StartCoroutine(HandleInvicibilityDelay());
            StartCoroutine(InvicibilityFlash());
        }
    }

    public void Die()
    {
        CharacBehavior charachBehavior = gameObject.GetComponent<CharacBehavior>();
        charachBehavior.StopPlayer();
        charachBehavior.enabled = false;
        GameOverManager.instance.OnPlayerDeath();
        //CharacBehavior.instance.enabled = false;
    }

    public void TakeHeal(int health)
    {
        if (currentHealth < 3)
        {
            currentHealth += health;
            healthBar.setHealth(currentHealth);
        }
    }

    public IEnumerator InvicibilityFlash()
    {
        while (isInvicible)
        {
            spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(0.2f);
            spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(0.2f);
        }
    }

    public IEnumerator HandleInvicibilityDelay()
    {
        isInvicible = true;
        yield return new WaitForSeconds(3f);
        isInvicible = false;
    }
}
