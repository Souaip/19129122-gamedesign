﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CharacBehavior : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator animator;
    public Vector3 velocity = Vector3.zero;
    public Vector2 TirDirection;
    public SpriteRenderer spriteRenderer;
    public float speed;
    public float puiJump;
    private bool isJump = false;
    private Vector3 childPos;
    public CharacBehavior instance;

    // Start is called before the first frame update
    void Start()
    {
        TirDirection = Vector2.right;
    }


    public void Awake()
    {
        if (instance != null)
        {
            print("Il y a plus d'une instance de CharacBehavior");
            return;
        }
        instance = this;
    }

    void Jump()
    {

        rb.AddForce(new Vector2(0, puiJump));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Box"))
        {
            isJump = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Box"))
        {
            isJump = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKeyDown("space") && isJump == false)
        {
            Jump();
        }

        float horizontalMovement = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        MovePlayer(horizontalMovement);

        childPos = transform.GetChild(0).position;
        if (rb.velocity.x < -0.3f && TirDirection != Vector2.left)
        {
            TirDirection = Vector2.left;
            transform.GetChild(0).transform.position = new Vector3(childPos.x - 2, transform.position.y, transform.position.z);
        }
        if (rb.velocity.x > 0.3f && TirDirection != Vector2.right)
        {
            TirDirection = Vector2.right;
            transform.GetChild(0).transform.position = new Vector3(childPos.x + 2, transform.position.y, transform.position.z);
        }
        if (rb.velocity.x < -0.3f)
        {
            spriteRenderer.flipX = true;

        }
        else if (rb.velocity.x > 0.3f)
        {
            spriteRenderer.flipX = false;
        }
        animator.SetFloat("SpeedPlayer", Math.Abs(rb.velocity.x));
    }

    void MovePlayer(float _horizontalMovement)
    {
        Vector3 targetVelocity = new Vector2(_horizontalMovement, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);
    }

    public void StopPlayer()
    {
        rb.velocity = new Vector3(0f, 0f, 0f);
        animator.SetFloat("SpeedPlayer", Math.Abs(rb.velocity.x));
    }
}
