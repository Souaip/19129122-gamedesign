﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    public int value;
    private GameObject[] Fish;

    void Start()
    {
        int Got = PlayerPrefs.GetInt("FishAmount" + value);
        if (Got == 1)
        {
            Fish = GameObject.FindGameObjectsWithTag("FishGet");
            Image FishRenderer = Fish[value].GetComponent<Image>();
            FishRenderer.color = new Color(1f, 1f, 1f, 1f);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            PlayerPrefs.SetInt("FishAmount" + value, 1);

            PlayerHealth player = collision.transform.GetComponent<PlayerHealth>();
            Fish = GameObject.FindGameObjectsWithTag("FishGet");
            Image FishRenderer = Fish[value].GetComponent<Image>();
            FishRenderer.color = new Color(1f, 1f, 1f, 1f);

            player.TakeHeal(1);
        }
    }
}
